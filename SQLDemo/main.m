//
//  main.m
//  SQLDemo
//
//  Created by Nic on 11/2/15.
//  Copyright (c) 2015 Nishant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
