//
//  ViewController.h
//  SQLDemo
//
//  Created by Nic on 11/2/15.
//  Copyright (c) 2015 Nishant. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"

@interface ViewController : UIViewController<UITextFieldDelegate>

@end
