//
//  ViewController.m
//  SQLDemo
//
//  Created by Nic on 11/2/15.
//  Copyright (c) 2015 Nishant. All rights reserved.
//

#import "ViewController.h"

#define RegNoTextfieldTag        100
#define NameTextfieldTag         101
#define DepartmentTextfieldTag   102
#define YearTextfieldTag         103
#define EnterRegNoTextfieldTag   104
#define SaveButtonTag            200
#define SearchButtonTag          201

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSArray *textFieldArray = @[@"Reg No", @"Name", @"Department", @"Year", @"Enter Reg No for search"];
    CGRect screenSize = [[UIScreen mainScreen] bounds];
    float xAxis = 30;
    float yAxis = 50;
    
    for (int i =0; i<[textFieldArray count]; i++)
    {
        UITextField *textFieldObject = [[UITextField alloc] initWithFrame:CGRectMake(xAxis, yAxis, screenSize.size.width-2*xAxis, 44)];
        if (i==4)
        {
            textFieldObject.frame = CGRectMake(xAxis, yAxis+90, screenSize.size.width-2*xAxis, 44);
        }
        textFieldObject.backgroundColor = [UIColor colorWithRed:46.0/255 green:160.0/255 blue:88.0/255 alpha:0.15];
        textFieldObject.tag = 100+i;
        textFieldObject.placeholder = [textFieldArray objectAtIndex:i];
        [textFieldObject setTextAlignment:NSTextAlignmentCenter];
        [self.view addSubview:textFieldObject];
        
        yAxis = yAxis + 10 + 44;
    }
    
    UIView *lineSepRatorView = [[UIView alloc] initWithFrame:CGRectMake(0, yAxis+12, screenSize.size.width, 1)];
    lineSepRatorView.backgroundColor = [UIColor orangeColor];
    [self.view addSubview:lineSepRatorView];
    
    yAxis = yAxis - 54;

    textFieldArray = @[@"Save", @"Search"];
    
    for (int i =0; i<[textFieldArray count]; i++)
    {
        if (i == 0 || i == 1)
        {
            UIButton *buttonObject = [[UIButton alloc] initWithFrame:CGRectMake(xAxis, yAxis, screenSize.size.width-2*xAxis, 44)];
            buttonObject.backgroundColor = [UIColor blueColor];
            buttonObject.tag = 200+i;
            [buttonObject setTitle:[textFieldArray objectAtIndex:i] forState:UIControlStateNormal];
            [buttonObject addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
            [self.view addSubview:buttonObject];
            
            yAxis = yAxis + 144;
        }
    }
}

- (void)buttonAction: (UIButton *)sender
{
    BOOL isPermission = false;
    NSString *message;

    UITextField *searchTextFieldRef     = (UITextField *)[self.view viewWithTag:EnterRegNoTextfieldTag];
    UITextField *regNoTextFieldRef      = (UITextField *)[self.view viewWithTag:RegNoTextfieldTag];
    UITextField *nameTextFieldRef       = (UITextField *)[self.view viewWithTag:NameTextfieldTag];
    UITextField *departmentTextFieldRef = (UITextField *)[self.view viewWithTag:DepartmentTextfieldTag];
    UITextField *yearTextFieldRef       = (UITextField *)[self.view viewWithTag:YearTextfieldTag];
    
    if (sender.tag == SaveButtonTag)
    {
        
        if (regNoTextFieldRef.text.length == 0)
        {
            isPermission = false;
            message = @"Reg number field can't be blank.";
        }
        else if (nameTextFieldRef.text.length == 0)
        {
            isPermission = false;
            message = @"Name field can't be blank.";
        }
        else if (departmentTextFieldRef.text.length == 0)
        {
            isPermission = false;
            message = @"Department field can't be blank.";
        }
        else if (yearTextFieldRef.text.length == 0)
        {
            isPermission = false;
            message = @"Year field can't be blank.";
        }
        else
        {
            isPermission = true;
        }
        
        if (isPermission){
            isPermission = [[DBManager getSharedInstance]saveData:regNoTextFieldRef.text
                                                             name:nameTextFieldRef.text
                                                       department:departmentTextFieldRef.text
                                                             year:yearTextFieldRef.text];

            if (isPermission == NO)
            {
                message = @"Data Insertion failed";
            }
            else
            {
                regNoTextFieldRef.text      = @"";
                nameTextFieldRef.text       = @"";
                departmentTextFieldRef.text = @"";
                yearTextFieldRef.text       = @"";
                
                message = @"Data Saved";
            }
        }
    }
    else if(sender.tag == SearchButtonTag)
    {
       
        if (searchTextFieldRef.text.length != 0)
        {
           
            NSArray *data = [[DBManager getSharedInstance]findByRegisterNumber:searchTextFieldRef.text];
            if (data == nil)
            {
                message = @"Data not found";
                isPermission = false;
            }
            else
            {
                message = @"Data found";
                isPermission = true;
                
                regNoTextFieldRef.text      = searchTextFieldRef.text;
                nameTextFieldRef.text       = [data objectAtIndex:0];
                departmentTextFieldRef.text = [data objectAtIndex:1];
                yearTextFieldRef.text       = [data objectAtIndex:2];
            }
        }
        else
        {
            message = @"Search field can't be blank.";
        }
    }
    
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@""
                                                   message:message
                                                  delegate:nil
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil];
    [alert show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    
    [self.view endEditing:true];
    return YES;
}
@end
